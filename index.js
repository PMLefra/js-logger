const fs = require('fs');

// Gets the current full year and the current week number
function getWeekNumber(d = new Date()) {
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
  let yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  let weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
  return [d.getUTCFullYear(), weekNo];
}

// Gets the time in hh:mm:ss format
function getHour(dt) {
  let d = dt || new Date();
  let dArr = [d.getHours(), d.getMinutes(), d.getSeconds()];
  for (let i = 0; i < 3; i++) {
    dArr[i] = dArr[i].toString();
    if (dArr[i].length === 1) dArr[i] = "0" + dArr[i];
  }
  return dArr.join(":");
}

// Main class
class Logger {
  constructor(logDirPath) {
    this.directory = logDirPath;
    if (this.directory[this.directory.length - 1] === '/')
      this.directory = this.directory.slice(0, -1);

    if (!fs.existsSync(this.directory))
      fs.mkdirSync(this.directory, {recursive: true});
  }

  /*
  * Main logging function.
  *
  * str: the text content, of type string
  * type: log category/type
  * user: an object whose property tag is a string representing the entity that caused the log
  * importance: integer, should be:
  * - 0 for unimportant / verbose
  * - 1 for worth keeping
  * - 2 for warning
  * - 3 for important / error
  * - 4 for critical
  */
  log(str = "", type = "", user = Logger.NONE, importance = Logger.VERBOSE) {
    // Logging everything in the console, but only importance > 0 in log files
    let importanceSymbol = Logger.symbols[importance];

    let consoleStr = `${getHour()} ${importanceSymbol}[${type.toUpperCase()}] (${user.tag}) ${str}`;
    console.log(consoleStr);

    if (importance > Logger.VERBOSE) {
      let fileStr = `${new Date().getTime()}${Logger.SEP}|${user.tag}${Logger.SEP}|${importanceSymbol} ${type}${Logger.SEP}|${str.replace(/\n/g, Logger.SEP + 'n')}\n`;
      let fileName = Logger.getCurrentLogFile(this.directory);
      fs.appendFileSync(fileName, fileStr, 'utf8');
    }
  }

  // Gets the name of the current log file for a given directory
  static getCurrentLogFile(directory) {
    let weekNumber = getWeekNumber();
    return `${directory}/log${weekNumber[1]}-${weekNumber[0] - 2000}.txt`;
  }

  // Creates a HTML version of the log txt file, in the same folder
  static toHTML(filePath = "") {
    function Log(date, user, type, content) {
      this.date = date;
      this.user = user;
      this.type = type;
      this.content = content;
    }

    if (filePath === "")
      filePath = Logger.getCurrentLogFile(__dirname);

    fs.readFile(filePath, (err, data) => {
      if (err) throw err;

      let logText = data.toString();

      //Début du document
      let htmlText = '<!DOCTYPE html>\n<html lang="fr"><head><meta charset="utf-8"/><title>Log file</title><style>';
      //CSS global
      htmlText += 'body {font-family: Lucida Console, DejaVu Sans Mono, monospace, sans-serif}';
      //CSS pour chaque log
      htmlText += '.date {color:gray; font-size:12px} .type {color:blue} .user {color:red} .content {margin:5px; font-size:20px; white-space: pre-wrap} .content:hover{background-color: rgb(230,230,230); cursor: text}';
      //CSS pour le holder des logs
      htmlText += '#logHolder div {border-bottom : 1px gray solid; padding-bottom: 10px} #logHolder div:hover{background-color: rgb(240,240,240)}';
      //Fin du head, début du body
      htmlText += '</style></head><body><div id="logHolder">';

      let logSplit = logText.split("\n");
      let logs = [];

      for (let i = 0; i < logSplit.length - 1; i++) {
        let infos = logSplit[i].split(Logger.SEP + "|");
        let actLog = new Log(infos[0], infos[1], infos[2], infos[3].split(Logger.SEP + "n").join("\n"));
        logs.push(actLog);
      }

      // Each log
      for (let log of logs) {
        htmlText += '<div>';
        htmlText += '<span class="date">';
        htmlText += new Date(Number(log.date)).toString();
        htmlText += '</span>';
        htmlText += '<br/>';
        htmlText += '<span class="user">';
        htmlText += log.user;
        htmlText += '</span>';
        htmlText += '<span>';
        htmlText += ' | ';
        htmlText += '</span>';
        htmlText += '<span class="type">';
        htmlText += log.type;
        htmlText += '</span>';
        htmlText += '<pre class="content" onclick="clip(this)">';
        htmlText += log.content;
        htmlText += '</pre>';
        htmlText += '</div>';
      }

      // Fin du holder, javascript de la sélection
      htmlText += '</div><script>';
      htmlText += 'var clip=function(e){var n=document.createRange();n.selectNodeContents(e);var a=window.getSelection();a.removeAllRanges(),a.addRange(n)};';
      // Fin du document
      htmlText += '</script></body></html>';

      let newPath = filePath.replace('.txt', '.html');

      fs.writeFile(newPath, htmlText, (err) => {
        if (err) throw err;
        console.log(`Log file '${filePath}' successfully turned to HTML log file '${newPath}'`);
      });
    });
  }
}

// String separator
Logger.SEP = "$~%";
// Importance levels
Logger.VERBOSE = 0;
Logger.INFO = 1;
Logger.WARNING = 2;
Logger.ERROR = 3;
Logger.CRITICAL = 4;
// Symbols and emojis used for each log importance level
Logger.symbols = [' ', '~', '⚠️', '🚫', '🔥'];
// Some useful logging entities
Logger.INTERNAL = {tag: "Internal"};
Logger.UNKNOWN = {tag: "Unknown"};
Logger.NONE = {tag: "None"};

module.exports = Logger;
